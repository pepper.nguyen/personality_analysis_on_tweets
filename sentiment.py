import csv
from textblob import TextBlob
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import Embedding
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras.layers import Dropout
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras import optimizers
from keras.models import load_model
from keras.callbacks import EarlyStopping
import numpy as np
from nltk.stem import WordNetLemmatizer
import operator
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import BernoulliNB
from nltk.tokenize import word_tokenize
from gensim.models import Word2Vec, KeyedVectors
from sklearn.metrics import classification_report
import pickle as pkl
from sklearn.preprocessing import LabelEncoder
import glob
import re
import os
import argparse

pattern = r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))|@[a-z0-9]*|\w*\d+\w*"
r = re.compile(pattern)

le = LabelEncoder()
lemma = WordNetLemmatizer()


# text = "what should i do now https://t.co/dx2vtowZG2 i clicked on the link and https://t.co/oCzD8qHFtR @hello"


def extract_data(user):
    '''

    :param user:
    :return:
    '''
    csv_user = "data/tweets_{}.csv".format(user)
    res = []
    with open(csv_user) as f:
        csv_reader = csv.reader(f, delimiter=';')
        for row in csv_reader:
            content = preprocessing(row[-1])
            res.append(content)
    del res[0]
    return res


def extract_data_kaggle(input_file):
    '''
    Extract data from kaggle dataset for further training
    :param input_file: kaggle dataset file
    :return: list of data and corresponding labels
    '''
    if os.path.exists("data/kaggle/label.pkl"):
        with open("data/kaggle/label.pkl", 'rb') as lab_f:
            sample_half_labs = pkl.load(lab_f)
        list_data_file = sorted(glob.glob(os.path.join('data/kaggle/', 'data*.pkl')))
        print(list_data_file)
        data_vec = list()
        for x in list_data_file:
            with open(x, 'rb') as dat_f:
                data_vec.extend(pkl.load(dat_f))
            print(len(data_vec))
    else:
        data = []
        labels = []
        with open(input_file, 'r', encoding="ISO-8859-1") as f:
            csv_reader = csv.reader(f, delimiter=',')
            for row in csv_reader:
                content = preprocessing(row[-1])
                if row[0] == '0':
                    polar = 0
                elif row[0] == '2':
                    polar = 1
                else:
                    polar = 2
                words_in_sentence = word_tokenize(content)
                if len(words_in_sentence):
                    data.append(words_in_sentence)
                    labels.append(polar)
        with open("data.temp", 'wb') as f:
            pkl.dump(data, f)
        sample_half_labs = labels[:100000]
        sample_half_labs.extend(labels[-100000:])
        labels_encode = le.fit_transform(sample_half_labs)
        data_vec = list()
        with open("data/kaggle/label.pkl", "wb") as f:
            pkl.dump(labels_encode, f)
        if not os.path.exists("models/w2v.bin"):
            vectorizer = Word2Vec(data, size=250, min_count=3, workers=4, )
            vectorizer.wv.save_word2vec_format("models/w2v.bin", binary=True)
        else:
            vectorizer = KeyedVectors.load_word2vec_format("models/w2v.bin", binary=True)
        sample_half = data[:100000]
        sample_half.extend(data[-100000:])
        count = 0
        for sen in sample_half:
            sum_feat = [0] * 250
            for x in sen:
                if x in vectorizer.wv.vocab:
                    sum_feat = list(map(operator.add, sum_feat, vectorizer[x]))
            sum_feat = list(map(lambda x: x / len(sen), sum_feat))
            data_vec.append(sum_feat)
            if len(data_vec) == 100000:
                with open("data/kaggle/data_part_{}.pkl".format(count + 1), 'wb') as f:
                    pkl.dump(data_vec, f)
                data_vec = []
                count += 1
        if len(data_vec):
            with open("data/kaggle/data_part_{}.pkl".format(count + 1), "wb") as f:
                pkl.dump(data_vec, f)
        del sample_half
        del data
        list_data_file = sorted(glob.glob(os.path.join('data/kaggle/', 'data*.pkl')))
        data_vec = list()
        for x in list_data_file:
            with open(x, 'rb') as dat_f:
                data_vec.extend(pkl.load(dat_f))
            print(len(data_vec))
    return data_vec, sample_half_labs


def preprocessing(sentence, concat=True):
    '''
    remove all the pattern in regex: tag, url, ..., lowercase, lemmatize then concat to a string
    :param sentence:
    :param concat: return concat string or keep it as a list of words
    :return:
    '''
    content = r.sub("", sentence)
    content = content.lower()
    res = [lemma.lemmatize(w) for w in word_tokenize(content)]
    if concat:
        return ' '.join(res)
    return res


def sentiment_textblob(user='', sentence='', whole=True):
    '''
    evaluate the overall user's sentiment by using Textblob
    :param user: user to analyze
    :param sentence: the text you want to analyze
    :param whole: True - if running on the whole joined document
                  False - if running by each tweet then count for finding dominance
    :return:
    '''
    if sentence != '':
        res = TextBlob(preprocessing(sentence, concat=True)).sentiment.polarity
        if -1 <= res < 0:
            return {"Result": "negative", "Polarity Score": res}
        elif 0 < res <= 1:
            return {"Result": "positive", "Polarity Score": res}
        else:
            return {"Result": "neutral", "Polarity Score": res}
    else:
        messages = extract_data(user)
        if whole:
            text = ". ".join(messages).lower()
            res = TextBlob(text.lower()).sentiment
            # print(res.polarity, res.subjectivity)
            if res.polarity > 0.15:
                return {"Result": "positive", "Polarity Score": res.polarity}
            elif -0.15 <= res.polarity <= 0.15:
                return {"Result": "neutral", "Polarity Score": res.polarity}
            else:
                return {"Result": "negative", "Polarity Score": res.polarity}
        else:
            polarities = []
            subjectivities = []
            for x in messages:
                res = TextBlob(x).sentiment
                polarities.append(res.polarity)
                subjectivities.append(res.subjectivity)
            pos_ratio = len([x for x in polarities if x > 0]) / len(messages)
            print(pos_ratio)
            if pos_ratio > 0.5:
                return {"Result": "positive"}
            elif 0.35 <= pos_ratio <= 0.5:
                return {"Result": "neutral"}
            else:
                return {"Result": "negative"}


def train_NB(data_file=''):
    X, Y = extract_data_kaggle(data_file)
    x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, shuffle=True)
    clf = BernoulliNB()
    # Fit data to model
    clf.fit(x_train, y_train)
    # Make predictions for the test set
    y_pred = clf.predict(x_test)
    # Write classification report
    print(classification_report(y_test, y_pred))
    with open("models/naive.pkl", "wb") as f:
        pkl.dump(clf, f)


def train_RF(data_file=''):
    X, Y = extract_data_kaggle(data_file)
    x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, shuffle=True, random_state=42)
    clf = RandomForestClassifier(max_depth=32, n_estimators=100)
    # Fit data to model
    clf.fit(x_train, y_train)
    # Make predictions for the test set
    y_pred = clf.predict(x_test)
    # Write classification report
    print(classification_report(y_test, y_pred))
    with open("models/forest.pkl", "wb") as f:
        pkl.dump(clf, f)


def pre_CNN(w2v, vocab):
    X, Y = extract_data_kaggle('')
    embeddings_index = {}
    for i in range(len(vocab)):
        word = list(vocab)[i]
        coefs = w2v[word]
        embeddings_index[word] = coefs
    with open("embedding_index.pkl", 'wb') as f:
        pkl.dump(embeddings_index, f)
    data = pkl.load(open('data.temp', 'rb'))
    sample_half = data[:100000]
    sample_half.extend(data[-100000:])

    data_string = list()
    for x in sample_half:
        data_string.append(' '.join(x))
    x_train, x_test, y_train, y_test = train_test_split(data_string, Y, test_size=0.2, shuffle=True)

    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(x_train)
    with open('models/tokenizer.pkl', 'wb') as f:
        pkl.dump(tokenizer, f)
    X_train = tokenizer.texts_to_sequences(x_train)
    X_test = tokenizer.texts_to_sequences(x_test)

    word_index = tokenizer.word_index
    vocab_size = len(word_index) + 1
    maxlen = 100
    X_train = pad_sequences(X_train, padding='post', maxlen=maxlen)
    X_test = pad_sequences(X_test, padding='post', maxlen=maxlen)
    embedding_weights = np.zeros((len(vocab), 250))

    for word, i in word_index.items():
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None:
            # words not found in embedding index will be all-zeros.
            embedding_weights[i] = embedding_vector

    return X_train, X_test, y_train, y_test, vocab_size, maxlen, embedding_weights


def init_NN_model(len_vocab, maxlen, embedding_weights):
    model = Sequential()
    model.add(Embedding(len_vocab, 250, input_length=maxlen, weights=[embedding_weights], trainable=False))
    model.add(Conv1D(16, 3, activation='relu', padding='same'))
    model.add(MaxPooling1D())
    model.add(Conv1D(32, 4, activation='relu', padding='same'))
    model.add(MaxPooling1D())
    model.add(Conv1D(64, 5, activation='relu', padding='same'))
    model.add(MaxPooling1D())
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))
    opt = optimizers.SGD(learning_rate=0.001)
    model.compile(optimizer=opt,
                  loss='binary_crossentropy',
                  metrics=['accuracy'])
    model.summary()
    return model


def train_CNN():
    vectorizer = KeyedVectors.load_word2vec_format("models/w2v.bin", binary=True)
    vocab = vectorizer.wv.vocab
    X_train, X_test, y_train, y_test, vocab_size, input_length, emb_weights = pre_CNN(vectorizer, vocab)
    model = init_NN_model(vocab_size, input_length, emb_weights)
    early = EarlyStopping(patience=3, monitor='val_loss', restore_best_weights=True)
    history = model.fit(X_train, y_train, epochs=30, verbose=True, batch_size=16, validation_data=(X_test, y_test),
                        callbacks=[early])
    model.save("cnn.h5")
    y_pred = model.predict_classes(X_test)
    print(classification_report(y_test, y_pred))


def sentiment_CNN(user='', sentence='', model_file="models/cnn.h5",
                  tokenizer_model='models/tokenizer.pkl'):
    with open(tokenizer_model, "rb") as f:
        tokenizer = pkl.load(f)
    model = load_model(model_file)
    if sentence == '':
        messages = extract_data(user)
        sentence = '. '.join(messages)
        sentence = preprocessing(sentence, concat=True)
    else:
        sentence = preprocessing(sentence, concat=True)
    # print(sentence)
    inp = pad_sequences(tokenizer.texts_to_sequences([sentence]), padding='post', maxlen=100)
    res = "negative"
    if model.predict_classes(inp)[0][0] == 1:
        res = "positive"
    return res


def sentiment_NaiveBayes(user='', sentence='', model_file="models/naive.pkl",
                         w2v_model='models/w2v.bin'):
    '''
    Evaluate the overall user's sentiment by using NaiveBayes. Training data is extracted from kaggle dataset
    :param user: user to analyze
    :param sentence: sentence to evaluate
    :param model_file: path to model file
    :param w2v_model: path to w2v model
    :return:
    '''
    label_parse = ['negative', 'positive']
    vectorizer = KeyedVectors.load_word2vec_format(w2v_model, binary=True)
    vocab = vectorizer.wv.vocab
    with open(model_file, "rb") as f:
        clf = pkl.load(f)
    if sentence == '':
        messages = extract_data(user)
        text = '. '.join(messages)
        word_list = preprocessing(text, concat=False)
    else:
        word_list = preprocessing(sentence, concat=False)
    sum_feat = [0] * 250
    for w in word_list:
        if w in vocab:
            sum_feat = list(map(operator.add, sum_feat, vectorizer[w]))
    sum_feat = list(map(lambda x: x / len(word_list), sum_feat))
    return label_parse[clf.predict([sum_feat])[0]]


def sentiment_RandomForest(user='', sentence='', model_file="models/forest.pkl",
                           w2v_model='models/w2v.bin'):
    '''
    Evaluate the overall user's sentiment by using Random Forest. Training data is extracted from kaggle dataset
    :param user: user to analyze
    :param sentence: sentence to evaluate
    :param model_file: path to model file
    :param w2v_model: path to w2v model
    :return:
    '''
    label_parse = ['negative', 'positive']
    vectorizer = KeyedVectors.load_word2vec_format(w2v_model, binary=True)
    vocab = vectorizer.wv.vocab
    with open(model_file, "rb") as f:
        clf = pkl.load(f)
    if sentence == '':
        messages = extract_data(user)
        text = '. '.join(messages)
        word_list = preprocessing(text, concat=False)
    else:
        word_list = preprocessing(sentence, concat=False)
    sum_feat = [0] * 250
    for w in word_list:
        if w in vocab:
            sum_feat = list(map(operator.add, sum_feat, vectorizer[w]))
    sum_feat = list(map(lambda x: x / len(word_list), sum_feat))
    return label_parse[clf.predict([sum_feat])[0]]


if __name__ == '__main__':
    print("Choose your model:")
    print("\t 0: TextBlob")
    print("\t 1: Naive Bayes")
    print("\t 2: Random Forest")
    print("\t 3: CNN")
    print("Answer: ")
    model = input()
    while model not in ['0', '1', '2', '3']:
        print("[ERROR]: Option out of range")
        print("Answer: ")
        model = input()
    print("Choose the analyze mode: ")
    print("\t 0: user")
    print("\t 1: sentence")
    print("Answer: ")
    mode = input()
    while mode not in ['0', '1']:
        print("[ERROR]: Option out of range")
        print("Answer: ")
        mode = input()
    if mode == '1':
        print("Enter your text here: ")
        txt = input()
        if model == '0':
            print("Result: ", sentiment_textblob(sentence=txt))
        elif model == '1':
            print("Result: ", sentiment_NaiveBayes(sentence=txt))
        elif model == '2':
            print("Result: ", sentiment_RandomForest(sentence=txt))
        else:
            print("Result: ", sentiment_CNN(sentence=txt))
    else:
        print("Enter your choice in list: ['dooce' ,'AndrewRosindell', 'Dr_Dan_1', 'lovespellx0x0', 'sockington']")
        print("Answer: ")
        user = input()
        if model == '0':
            print("Choose processing mode: ")
            print("\t 0: whole text")
            print("\t 1: sentence by sentence")
            pmode = input()
            while pmode not in ['0', '1']:
                print("[ERROR]: Option out of range")
                print("Answer: ")
                pmode = input()
            if pmode == '0':
                print("Result: ", sentiment_textblob(user=user, whole=True))
            else:
                print("Result: ", sentiment_textblob(user=user, whole=False))
        elif model == '1':
            print("Result: ", sentiment_NaiveBayes(user=user))
        elif model == '2':
            print("Result: ", sentiment_RandomForest(user=user))
        else:
            print("Result: ", sentiment_CNN(user=user))

