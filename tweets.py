# sockington, dooce, lovespellx0x0, Dr_Dan_1, dog_feelings, AndrewRosindell, 3percentsamoan
'''
python tweets.py -u xxxxx -c 3200
'''

import tweepy
from config import CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET
import csv
import os
import argparse
import time


def clone_tweets(api, user, nb_tweets):
    '''
    clone tweets with specific userid and wanted number
    :param api: twitter API (already initialized once to save the processing time)
    :param user: account holder to be cloned, type=str
    :param nb_tweets: number of wanted tweets, type=int, max=3200
    :return:
    '''
    start = time.time()
    tweets = tweepy.Cursor(api.user_timeline, id=user, include_rts=False, tweet_mode="extended").items(nb_tweets)
    clone_list = [[tw.user.screen_name, tw.created_at, tw.id, tw.full_text] for tw in tweets]
    print(time.time() - start)
    print(len(clone_list))
    if not os.path.exists("data"):
        os.mkdir("data")
    outfile = "data/tweets_{0}.csv".format(user)
    save_by_users(clone_list, outfile)


def save_by_users(cloned_tweets, out_file):
    '''
    sort tweets in order of timestamp then save to csv file with delimiter ;
    :param cloned_tweets: the list of cloned tweets
    :param out_file: output filename, where store all the cloned tweets from an user
    :return:
    '''
    sorted_tweets = sorted(cloned_tweets, key=lambda k: k[2], reverse=False)
    print("[INFO]: Saving tweets to {0}".format(out_file))
    with open(out_file, 'w+') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerow(["userid", "created_at", "tweet_id", "message"])
        writer.writerows(sorted_tweets)
    print("[INFO]: Saved successfully ")


def env_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--user', '-u', type=str, required=True, help='clone from user with id')
    parser.add_argument('--count', '-c', type=int, required=True, help='number of tweets')
    return vars(parser.parse_args())


if __name__ == '__main__':
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
    tweets_api = tweepy.API(auth)
    args = env_parser()
    clone_tweets(tweets_api, args['user'], args['count'])