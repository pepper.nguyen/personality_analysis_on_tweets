import csv
import joblib
from keras.layers import Input, concatenate, Dropout
from keras.layers import Conv3D, MaxPooling3D, Dense, Reshape
from keras.models import Model, Sequential
from conv_net_train_keras import make_idx_data_cv, Generator

import logging


def load_final_model(embbeding_size=300, sent_max_count=312, word_max_count=153, weight_path=''):
    # define model architecture
    model_input = Input(shape=(sent_max_count, word_max_count, embbeding_size, 1), name='main_input')

    # unigram
    model_1 = Sequential()
    model_1.add(Conv3D(200, (1, 1, embbeding_size), activation='relu',
                       input_shape=(sent_max_count, word_max_count, embbeding_size, 1)))
    model_1.add(MaxPooling3D((1, word_max_count, 1)))

    model_output_1 = model_1(model_input)

    # bigrams
    model_2 = Sequential()
    model_2.add(Conv3D(200, (1, 2, embbeding_size), activation='relu',
                       input_shape=(sent_max_count, word_max_count, embbeding_size, 1)))
    model_2.add(MaxPooling3D((1, word_max_count - 1, 1)))

    model_output_2 = model_2(model_input)

    # trigrams
    model_3 = Sequential()
    model_3.add(Conv3D(200, (1, 3, embbeding_size), activation='relu',
                       input_shape=(sent_max_count, word_max_count, embbeding_size, 1)))
    model_3.add(MaxPooling3D((1, word_max_count - 2, 1)))

    model_output_3 = model_3(model_input)

    model = concatenate([model_output_1, model_output_2, model_output_3], axis=-1)

    after_MaxPooling = MaxPooling3D((sent_max_count, 1, 1))(model)

    mairesse_input = Input(shape=(84,), name='mairesse')
    model = Reshape((600,))(after_MaxPooling)
    concatenated_with_mairsse = concatenate([model, mairesse_input], axis=-1)

    model = Dense(200, activation='sigmoid')(concatenated_with_mairsse)
    model = Dropout(0.5)(model)
    output = Dense(2, activation='softmax')(model)

    final_model = Model(inputs=[model_input, mairesse_input], outputs=output)
    final_model.compile(optimizer='adadelta', loss='categorical_crossentropy', metrics=['accuracy'])

    final_model.summary()
    final_model.load_weights(weight_path)
    return final_model


if __name__ == "__main__":
    x = joblib.load("essays_mairesse.p")

    revs, W, W2, word_idx_map, vocab, mairesse = x[0], x[1], x[2], x[3], x[4], x[5]
    logging.info("data loaded!")

    charged_words = []

    emof = open("Emotion_Lexicon.csv", "rt")

    csvf = csv.reader(emof, delimiter=',', quotechar='"')
    first_line = True

    for line in csvf:
        if first_line:
            first_line = False
            continue
        if line[11] == "1":
            charged_words.append(line[0])

    emof.close()

    charged_words = set(charged_words)
    model = load_final_model(weight_path='mod-0.52.hdf5')
    attr = 4
    datasets = make_idx_data_cv(revs, word_idx_map, mairesse, charged_words, 1, attr, max_l=149,
                                max_s=312, k=300,
                                filter_h=3)

    X_train = datasets[0][:1]
    y_train = datasets[1][:1]
    mairesse_train = datasets[4][:1]
    word_max_count = 153
    sent_max_count = 312
    embedding_size = 300
    one_data_G = Generator(X_train, mairesse_train, y_train, 16, W, sent_max_count, word_max_count,
                           embedding_size)

    print(model.predict(one_data_G))
