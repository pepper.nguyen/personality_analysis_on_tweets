import sentiment
import personality
import numpy as np
import scipy.stats

x_dooce = sentiment.sentiment_textblob(user='dooce')
y_dooce = model.predict(one_data_G)
pearson_dooce = scipy.stats.pearsonr(x_dooce, y_dooce)       #returns both Pearson's r and the p-value

x_andrew = sentiment.sentiment_textblob(user='AndrewRosindell')
y_andrew = model.predict(one_data_G)
pearson_andrew = scipy.stats.pearsonr(x_andrew, y_andrew)

x_dan = sentiment.sentiment_textblob(user='Dr_Dan_1')
y_dan = model.predict(one_data_G)
pearson_dan = scipy.stats.pearsonr(x_dan, y_dan)

x_lovespell = sentiment.sentiment_textblob(user='lovespellx0x0')
y_lovespell =m odel.predict(one_data_G)
pearson_lovespell = scipy.stats.pearsonr(x_lovespell, y_lovespell)

x_sockington = sentiment.sentiment_textblob(user='sockington')
y_sockington = model.predict(one_data_G)
pearson_sockington = scipy.stats.pearsonr(x_sockington, y_sockington)

print(f"Pearson's r and p-value for the user dooce: {pearson_dooce}")
print(f"Pearson's r and p-value for the user AndrewRosindell: {pearson_andrew}")
print(f"Pearson's r and p-value for the user Dr_Dan_1: {pearson_dan}")
print(f"Pearson's r and p-value for the user lovespellx0x0: {pearson_lovespell}")
print(f"Pearson's r and p-value for the user sockington: {pearson_sockington}")