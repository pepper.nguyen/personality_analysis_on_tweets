# Personality Traits and Sentiment Analysis

This repository has been tested on Python 3.7

### Usages
- Install all packages (ordinary way)
```
pip install -r requirements.txt
```
- If you are using Anaconda, run this command to create the environment:
```
conda env create -f project20.yml
```
_(Remember to activate your environment afterwards)_
- To run sentimental model, run:
```
python sentiment.py

```
- To see the prediction output of personality model, run:
```
python personality.py
```
- To test Twitter API:
```
python tweets.py -u username -c 3200
```
you can change the username to your interested account and 3200 (default as max) to your wanted number of tweets.

- To run the pearson's R calculations:
```
python pearson.py
```

### Trained models
- Sentiment model: [Download link](https://drive.google.com/drive/folders/1uO1ZxiGWOyFPUcnax-zdohRUQqEQzrm4?usp=sharing)
- Personality traits: [Download link](https://drive.google.com/drive/folders/11IFw_uolTpGrT0G6RR0Yz2OOMr6NvTiw?usp=sharing)

### Contact
For any further questions related to this project (how to use, API access key, token, etc.,
), please do not hesitate to send us an email by **hunguyen20@student.oulu.fi** or **srytinki20@student.oulu.fi** for more information.
 
